//=============================================================================
// IPCV - Scilab Image Processing and Computer Vision toolbox
// Copyright (C) 2017  Tan Chin Luh
//=============================================================================
function [] = impixel_event(win, x, y, ibut)

    if ibut == (-1000) then   return,end,
    [x,y] = xchange(x, y, 'i2f');
    
    //if ibut == 10 then
        
        //rep=xgetmouse();
        //x=rep(1);y=rep(2);

        if ibut == 3 then
            f = gcf();
            S = f.children.children.data;
            [r,c] = size(S);
            y = r + 1 - y;
            if x>=1 & x<=c & y>=1 & y<=r    
                val = S(round(y),round(x), :);
                if  length(val) == 3
                    if type(val(1)) == 1
                        xinfo(sprintf('RGB at (%i,%i) = [%f %f %f]', x, y,val(1),val(2),val(3)));
                    else
                        xinfo(sprintf('RGB at (%i,%i) = [%i %i %i]', x, y,val(1),val(2),val(3)));
                    end
                else
                    if type(val(1)) == 1
                        xinfo(sprintf('Intensity at (%i,%i) = [%f]', x, y,val));    
                    else
                        xinfo(sprintf('Intensity at (%i,%i) = [%i]', x, y,val));
                    end
                end
                
            else
                if  length(val) == 3
                    xinfo(sprintf('RGB at (%i,%i) = [%f %f %f]', x, y,%nan,%nan,%nan));
                else
                    xinfo(sprintf('Intensity at (%i,%i) = [%f]', x, y,%nan));
                end                
                
            end
        end      
    //end




    //xinfo(msprintf('RGB (%f,%f,%f)',val(1),val(2),val(3)))
endfunction
