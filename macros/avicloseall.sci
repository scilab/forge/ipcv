// function avicloseall
//    Close all opened video files/cameras. (experimental)
//    
//    Syntax
//    avicloseall()
//    
//    Description
//    avicloseall close all opened video files or cameras.
//    
//    Video support for IPCV is only available when IPCV is compiled with OpenCV which support video I/O.
//    
//    Examples
//    n = aviopen(fullpath(getIPCVpath() + "/images/video.avi"));
//    im = avireadframe(n); //get a frame
//    imshow(im);
//    
//    avilistopened()
//    aviclose(n);
//     
//    See also
//    aviinfo
//    aviopen
//    camopen
//    avifile
//    addframe
//    aviclose
//    avilistopened
//    avireadframe 
//    
//    Authors
//    Shiqi Yu
//    Tan Chin Luh

