function y = imfinfo(x)
    
    y = int_imfinfo(x);
    s = imread(x);
    if isbw(s)
        y.ColorType = "binary";
    end
    imfinfo_print(y);
    
endfunction
