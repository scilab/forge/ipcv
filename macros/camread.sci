// function camread
//    Grabs and returns a frame from a camera(experimental)
//    
//    Syntax
//    im = camread(n, [w h])
//    
//    Parameters
//    n : The opened video file/camera index.
//    im : The returned frame/image. If no frame, return 0.
//    w, h : Width and Height of the image.
//    
//    Description
//    camread grabs and returns a frame from a camera.
//    
//    Video support for IPCV is only available when IPCV is compiled with OpenCV which support video I/O.
//    
//    Examples
//    n = camopen(0);
//    im = camread(n); //get a frame
//    imshow(im);
//    avicloseall();
//     
//    See also
//    aviinfo
//    aviopen
//    camopen
//    avifile
//    addframe
//    aviclose
//    avicloseall
//    avilistopened
//    
//    Authors
//    Tan Chin Luh

