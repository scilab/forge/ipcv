//=============================================================================
// IPCV - Scilab Image Processing and Computer Vision toolbox
// Copyright (C) 2017  Tan Chin Luh
//=============================================================================
function fobj = imdetect_DENSE(im,initFeatureScale,featureScaleLevels,featureScaleMul,initXyStep,initImgBound,varyXyStepWithScale,varyImgBoundWithScale);
    // Detect features from an image with DENSE algorithm
    //
    // Syntax
    //     fobj = imdetect_DENSE(im [,initFeatureScale [,featureScaleLevels [,featureScaleMul [,initXyStep [,initImgBound [,varyXyStepWithScale [,varyImgBoundWithScale]]]]]]]);
    //
    // Parameters
    //     im : Input image
    //     initFeatureScale : Default value is 1.
    //     featureScaleLevels : Default value is 1.
    //     featureScaleMul : Default value is 0.1.
    //     initXyStep : Default value is 6.
    //     initImgBound : Default value is 0.
    //     varyXyStepWithScale : Default value is 1.
    //     varyImgBoundWithScale : Default value is 0.
    //     fobj : Features object contains following fields -
    //          type : Type of features
    //          n : Numbers of detected features
    //          x : Coordinates of the detected features - X
    //          y : Coordinates of the detected features - Y
    //          size : Size of detected features
    //          angle : keypoint orientation
    //          response : The response by which the most strong keypoints have been selected.
    //          octave : pyramid octave in which the keypoint has been detected
    //          class_id : object id
    //          
    // Description
    //    This function used to detect the features of an image using DENSE method
    //
    // Examples
    //    S = imcreatechecker(8,8,[1 0.5]);
    //    fobj = imdetect_DENSE(S);
    //    imshow(S); plotfeature(fobj);
    //
    // See also
    //     imdetect_FAST
    //     imdetect_STAR
    //     imdetect_SIFT 
    //     imdetect_SURF
    //     imdetect_ORB
    //     imdetect_BRISK
    //     imdetect_MSER
    //     imdetect_GFTT
    //     imdetect_HARRIS
    //     imdetect_DENSE
    //     plotfeature
    //
    // Authors
    //    Tan Chin Luh
    //
    // Bibliography
    //    1. OpenCV 2.4 Online Documentation

    //

    rhs=argn(2);

    // Error Checking
    if rhs < 1; error("At least 1 argument expected, input image"); end    
    if rhs < 2; initFeatureScale = 1; end
    if rhs < 3; featureScaleLevels = 1; end
    if rhs < 4; featureScaleMul = 0.1; end
    if rhs < 5; initXyStep = 6; end
    if rhs < 6; initImgBound = 0; end
    if rhs < 7; varyXyStepWithScale = 1; end
    if rhs < 8; varyImgBoundWithScale = 0; end  
             
    if isempty(initFeatureScale); initFeatureScale = 1; end
    if isempty(featureScaleLevels); featureScaleLevels = 1; end
    if isempty(featureScaleMul); featureScaleMul = 0.1; end
    if isempty(initXyStep); initXyStep = 6; end
    if isempty(initImgBound); initImgBound = 0; end
    if isempty(varyXyStepWithScale); varyXyStepWithScale = 1; end
    if isempty(varyImgBoundWithScale); varyImgBoundWithScale = 0; end  
                          
    checkrange(2,initFeatureScale,0,32767);   // TBC
    checkrange(3,featureScaleLevels,0,32767);
    checkrange(4,featureScaleMul,0,32767); 
    checkrange(5,initXyStep,0,32767);  
    checkrange(6,initImgBound,0,32767);  
    checkrange(7,varyXyStepWithScale,0,1); 
    checkrange(8,varyImgBoundWithScale,0,1); 
      
    if typeof(im) ~= 'uint8'
        im = im2uint8(im);
    end

    r = int_imdetect_DENSE(im,initFeatureScale,featureScaleLevels,featureScaleMul,initXyStep,initImgBound,varyXyStepWithScale,varyImgBoundWithScale);

    fobj.type = 'DENSE';
    fobj.n = size(r,2);
    fobj.x = r(1,:);
    fobj.y = r(2,:);
    fobj.size = r(3,:);
    fobj.angle = r(4,:);
    fobj.response = r(5,:);
    fobj.octave = r(6,:);
    fobj.class_id = r(7,:);

endfunction










