// function camopen
//    Open a camera. (experimental)
//    
//    Syntax
//    n = camopen(i)
//    
//    Parameters
//    i : The i'th camera.
//    n : A number, the opened video file/camera index.
//    
//    Description
//    camopen open a camera, but it does not read frames from the camera. Please use im=camread(n) to get a frame from the n'th opened video file. Remember to close the opened camera using aviclose(n) or avicloseall() .
//    Video support for IPCV is only available when IPCV is compiled with OpenCV which support video I/O.
//        
//    Examples
//    n = camopen(0);
//    im = camread(n); //get a frame
//    imshow(im);
//    avicloseall();
//      
//    See also
//    aviinfo
//    aviopen
//    avifile
//    addframe
//    aviclose
//    avicloseall
//    avilistopened
//    avireadframe
//    
//    Authors
//    Shiqi Yu
//    Tan Chin Luh
//
